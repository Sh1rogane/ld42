﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    private Transform t;
    private float toAngle = 0;
	// Use this for initialization
	void Start ()
    {
        this.t = transform;

        t.DOMoveY(0, 3f);
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.E))
        {
            toAngle -= 90;
            if(toAngle < 0)
            {
                toAngle += 360;
            }
            t.DOLocalRotate(new Vector3(0, toAngle, 0), 0.5f, RotateMode.Fast);
        }
        else if(Input.GetKeyDown(KeyCode.Q))
        {
            toAngle += 90;
            if(toAngle > 360)
            {
                toAngle -= 360;
            }
            t.DOLocalRotate(new Vector3(0, toAngle, 0), 0.5f, RotateMode.Fast);
        }
	}
}
