﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class MenuController : MonoBehaviour
{
    public Toggle wave1Toggle;
    public Toggle wave5Toggle;
    public Toggle wave10Toggle;

    public CanvasGroup canvasGroup;
    public CanvasGroup instructionGroup;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void onPlayBtn()
    {
        if(wave1Toggle.isOn)
        {
            Globals.startFromWave = 0;
        } 
        else if(wave5Toggle.isOn)
        {
            Globals.startFromWave = 4;
        }
        if(wave10Toggle.isOn)
        {
            Globals.startFromWave = 9;
        }

        canvasGroup.DOFade(0, 1).OnComplete(onComplete);
    }
    public void onInstructionBtn()
    {
        instructionGroup.interactable = true;
        instructionGroup.blocksRaycasts = true;
        instructionGroup.DOFade(1, 0.5f);
    }
    public void onBack()
    {
        instructionGroup.interactable = false;
        instructionGroup.blocksRaycasts = false;
        instructionGroup.DOFade(0, 0.5f);
    }

    private void onComplete()
    {
        SceneManager.LoadScene("Game");
    }
    
}
