﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class BigEnemy : MonoBehaviour, EnemyHealth
{
    public GameObject blockPrefab;

    private Material mat;

    public int health = 40;
    private int maxHealth;
    private bool dead;

	// Use this for initialization
	void Start ()
    {
        maxHealth = health;

        mat = GetComponentInChildren<Renderer>().material;
        Color currentColor = Globals.enemyColor;
        currentColor.a = 0;

        mat.color = currentColor;
        mat.DOFade(1, 0.5f);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void takeDamage(int damage)
    {
        if(!dead)
        {
            health--;
            mat.color = Color.Lerp(Globals.enemyColor, Color.black, 1 - (float)health / maxHealth);
            if(health <= 0 && !dead)
            {
                dead = true;
                Destroy(gameObject);


                Instantiate(blockPrefab, transform.position + new Vector3(0.5f, 0, 0.5f), transform.rotation);
                Instantiate(blockPrefab, transform.position + new Vector3(-0.5f, 0, 0.5f), transform.rotation);
                Instantiate(blockPrefab, transform.position + new Vector3(0.5f, 0, -0.5f), transform.rotation);
                Instantiate(blockPrefab, transform.position + new Vector3(-0.5f, 0, -0.5f), transform.rotation);
            }
        }
        
    }
}
