﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateSelf : MonoBehaviour
{
    private Transform t;

    public Vector3 rotateVector;
    public Space space;
	// Use this for initialization
	void Start () {
        this.t = transform;
	}
	
	// Update is called once per frame
	void Update () {
        this.t.Rotate(rotateVector * Time.deltaTime, space);
	}
}
