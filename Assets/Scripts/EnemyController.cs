﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class EnemyController : MonoBehaviour
{
    public static EnemyController instance;

    public GameObject basicEnemyPrefab;
    public GameObject bigEnemyPrefab;
    public GameObject diggingEnemyPrefab;

    public Text waveLabel;
    public GameObject nextWaveGo;
    public Text nextWaveLabel;
    public CanvasGroup canvasGroup;

    public List<Transform> entryPoints = new List<Transform>();
    public List<Transform> diggEntryPoints = new List<Transform>();

    public int wave = 0;

    private float waveTimer = 0;

    private Queue<SpawnEntry> waveQueue = new Queue<SpawnEntry>();
    private SpawnEntry currentEntry;

    private bool waveFinished = true;

    private float waitTimer;
    private float waitTime = 10;

	// Use this for initialization
	void Start ()
    {
        instance = this;

        canvasGroup.DOFade(1, 3f);
        //createWave();
        wave = Globals.startFromWave;
        waveLabel.text = "Wave " + (Globals.startFromWave + 1);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!PlayerController.instance.dead)
        {
            if(!waveFinished)
            {
                waveTimer += Time.deltaTime;
                if(waveTimer >= currentEntry.spawnTime)
                {
                    waveTimer = 0;
                    spawnEnemy(currentEntry);

                    if(waveQueue.Count > 0)
                    {
                        currentEntry = waveQueue.Dequeue();
                    }
                    else
                    {
                        waveFinished = true;
                    }
                }
            }
            else
            {
                nextWaveGo.SetActive(true);
                nextWaveLabel.text = (10 - waitTimer).ToString("F1");

                waitTimer += Time.deltaTime;
                if(waitTimer >= waitTime)
                {
                    waitTimer = 0;
                    nextWave();
                }
            }
        }       
	}

    private void spawnEnemy(SpawnEntry entry)
    {
        GameObject spawn;

        switch(entry.enemyType)
        {
            case 0:
                spawn = basicEnemyPrefab;
                break;
            case 1:
                spawn = bigEnemyPrefab;
                break;
            case 2:
                spawn = diggingEnemyPrefab;
                break;
            default:
                spawn = basicEnemyPrefab;
                break;
        }

        if(entry.enemyType == 2)
        {
            Vector3 pos = diggEntryPoints[Random.Range(0, diggEntryPoints.Count)].position;
            Instantiate(spawn, pos, entry.entryRotation);
        }
        else
        {
            Instantiate(spawn, entry.entryPos, entry.entryRotation);
        }

        
    }
    private void nextWave()
    {
        nextWaveGo.SetActive(false); 

        wave++;
        waveLabel.text = "Wave " + wave; 

        if(wave <= 5)
        {
            for(int i = 0; i < wave; i++)
            {
                int enemyType = 0;
                int spawnPoint = Random.Range(0, 4);
                float spawnTime = Random.Range(3, 8);

                waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
            }
        }
        else if(wave == 6)
        {
            int enemyType = 1;
            int spawnPoint = Random.Range(0, 4);
            float spawnTime = Random.Range(1, 5);

            waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
        }
        
        else if(wave <= 10)
        {
            for(int i = 0; i < 6; i++)
            {
                int enemyType = Random.Range(0, 3);
                int spawnPoint = Random.Range(0, 4);
                float spawnTime = Random.Range(3, 8);

                waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
            }
        }
        else if(wave == 11)
        {
            for(int i = 0; i < 4; i++)
            {
                int enemyType = 1;
                int spawnPoint = i;
                float spawnTime = i;

                waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
            }
        }
        else if(wave == 12)
        {
            for(int i = 0; i < 4; i++)
            {
                int enemyType = 2;
                int spawnPoint = i;
                float spawnTime = 1;

                waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
            }
        }
        else if(wave <= 15)
        {
            for(int i = 0; i < 6; i++)
            {
                int enemyType = Random.Range(0, 3);
                int spawnPoint = Random.Range(0, 4);
                
                float spawnTime = Random.Range(3, 5);

                if(i % 2 == 0)
                {
                    spawnTime = 0;
                }

                waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
            }
        }
        else
        {
            for(int i = 0; i < 10; i++)
            {
                int enemyType = Random.Range(0, 3);
                int spawnPoint = Random.Range(0, 4);
                float spawnTime = Random.Range(1, 5);

                if(i % 2 == 0)
                {
                    spawnTime = 0;
                }

                waveQueue.Enqueue(new SpawnEntry(enemyType, spawnTime, entryPoints[spawnPoint]));
            }
        }

        waveFinished = false;
        currentEntry = waveQueue.Dequeue();
    }

    private struct SpawnEntry
    {
        public int enemyType;
        public float spawnTime;
        public Vector3 entryPos;
        public Quaternion entryRotation;

        public SpawnEntry(int enemyType, float spawnTime, Transform transform)
        {
            this.enemyType = enemyType;
            this.spawnTime = spawnTime;
            this.entryPos = transform.position;
            this.entryRotation = transform.rotation;
        }
    }
}
