﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DiggingEnemy : MonoBehaviour, EnemyHealth
{
    public GameObject blockPrefab;

    private Rigidbody rigidbody;
    private FollowPlayer followPlayer;
    public GameObject cube;

    private bool isDigging = true;

    private Material mat;
    private Color currentColor;

    public int health = 10;
    private int maxHealth;
    private bool dead = false;
    // Use this for initialization
    void Start ()
    {
        maxHealth = health;
        mat = GetComponentInChildren<Renderer>().material;

        rigidbody = GetComponent<Rigidbody>();
        followPlayer = GetComponent<FollowPlayer>();

        rigidbody.isKinematic = true;
        followPlayer.enabled = false;

        transform.DOMoveY(0.5f, 3).SetEase(Ease.Linear).OnComplete(onComplete);
    }
	private void onComplete()
    {
        rigidbody.isKinematic = false;
        followPlayer.enabled = true;
        isDigging = false;
        cube.transform.localPosition = new Vector3(0, 0.5f, 0);
    }
    // Update is called once per frame
    void Update ()
    {
        if(isDigging)
        {
            cube.transform.localPosition = Vector3.Lerp(cube.transform.localPosition, new Vector3(0, 0.5f, 0f) + Random.insideUnitSphere * 0.3f, Time.deltaTime * 10);
        }
    }

    public void takeDamage(int damage)
    {
        if(!dead)
        {
            health--;
            mat.color = Color.Lerp(Globals.enemyColor, Color.black, 1 - (float)health / maxHealth);
            if(health <= 0)
            {
                dead = true;
                Destroy(gameObject);
                GameObject go = Instantiate(blockPrefab, transform.position, transform.rotation);
            }
        }

    }
}
