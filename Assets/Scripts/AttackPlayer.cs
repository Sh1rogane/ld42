﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackPlayer : MonoBehaviour
{
    public int damage = 1;
    public float attackTime = 0.1f;
    private float attackTimer = 0;

    private bool inAttackRange;
	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(inAttackRange)
        {
            attackTimer += Time.deltaTime;
            if(attackTimer >= attackTime)
            {
                attackTimer = 0;
                attack();
            }
            
        }
	}
    private void attack()
    {
        PlayerController.instance.takeDamage(damage);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            inAttackRange = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            inAttackRange = false;
        }
    }
}
