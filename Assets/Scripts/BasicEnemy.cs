﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BasicEnemy : MonoBehaviour, EnemyHealth
{
    public GameObject blockPrefab;

    private Material mat;
    private Color currentColor;

    public int health = 10;
    private int maxHealth;
    private bool dead = false;
	// Use this for initialization
	void Start ()
    {
        maxHealth = health;
        mat = GetComponentInChildren<Renderer>().material;

        currentColor = Globals.enemyColor;
        currentColor.a = 0;

        mat.color = currentColor;

        mat.DOFade(1, 0.5f);
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void takeDamage(int damage)
    {
        if(!dead)
        {
            health--;
            mat.color = Color.Lerp(Globals.enemyColor, Color.black, 1 - (float)health / maxHealth);
            if(health <= 0)
            {
                dead = true;
                Destroy(gameObject);
                GameObject go = Instantiate(blockPrefab, transform.position, transform.rotation);
            }
        }
        
    }
}
