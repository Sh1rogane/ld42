﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private Transform t;

	// Use this for initialization
	void Start ()
    {
        t = transform;

        Destroy(gameObject, 3f);
	}
	
	// Update is called once per frame
	void Update ()
    {
        t.position += t.forward * 50 * Time.deltaTime;
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Block" || collision.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
        }
        else if(collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
            EnemyHealth eh = collision.gameObject.GetComponentInParent<EnemyHealth>();
            if(eh != null)
            {
                eh.takeDamage(1);
            }
        }
    }
}
