﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private Rigidbody rigidbody;

    public float speed = 2;


	// Use this for initialization
	void Start () {
        this.rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 dir = PlayerController.instance.transform.position - transform.position;
        dir.y = 0;
        dir.Normalize();

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * 180);

        rigidbody.velocity = transform.forward * speed;
	}
}
