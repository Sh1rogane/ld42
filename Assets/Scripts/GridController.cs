﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridController : MonoBehaviour
{
    [ContextMenu("Generate grid")]
    public void generateGrid()
    {
        gridPoints = new GridPoint[18, 18];

        for(int x = 0; x < 18; x++)
        {
            for(int z = 0; z < 18; z++)
            {
                gridPoints[x, z] = new GridPoint(x, z);
            }
        }
    }
    [SerializeField]
    private GridPoint[,] gridPoints = new GridPoint[18,18];

    public static GridController instance;

	// Use this for initialization
	void Start ()
    {
        generateGrid();

        instance = this;
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnDrawGizmos()
    {
        for(int x = 0; x < gridPoints.GetLength(0); x++)
        {
            for(int z = 0; z < gridPoints.GetLength(1); z++)
            {
                gridPoints[x, z].drawGizmos(transform);
            }
        }
    }

    [System.Serializable]
    public struct GridPoint
    {
        public int x;
        public int z;

        public bool occupied;

        public GridPoint(int x, int z)
        {
            this.x = x;
            this.z = z;
            occupied = false;
        }

        public void drawGizmos(Transform parent)
        {
            if(occupied)
            {
                Gizmos.color = Color.red;
            }
            else
            {
                Gizmos.color = Color.green;
            }

            Gizmos.DrawWireCube(new Vector3(x + 0.5f, 0.01f, z + 0.5f) + parent.position, new Vector3(1, 0, 1));
            
        }
    }
}
