﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    public Bullet bulletPrefab;
    [Header("Weapon")]
    public Transform weaponTransform;
    public Transform firePoint;
    public AudioSource shotgunAudio;
    public AudioClip shotgunFire;
    public AudioClip shotgunReload;

    [Header("Other")]
    public AudioSource hurtAudio;

    [Header("UI")]
    public Text healthLabel;
    public Text ammoLabel;
    public RectTransform gameOverGroup;
    public Text waveLabel;

    public Transform carryPoint;

    public Shredder shredder;

    private Transform t;
    private Camera cam;

    public float speed = 5;
    public float carrySpeedMul = 0.7f;
    public int health = 100;

    private bool isCarrying;
    private float carrySpeed = 1;

    private bool isReloading;
    private int maxAmmo = 6;
    private int currentAmmo = 6;

    private float fireTime = 0.5f;
    private float fireTimer = 0.5f;

    private float reloadTime = 0.2f;
    private float reloadTimer = 0f;

    private CharacterController cc;

    private bool insideShredder = false;
    private bool insideBlock = false;

    private GameObject carryBlock;

    public bool dead;
	// Use this for initialization
	void Start ()
    {
        instance = this;

        this.t = transform;
        cam = Camera.main;

        cc = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.K))
        {
            ScreenCapture.CaptureScreenshot("Capture.png");
        }
        if(dead)
        {
            return;
        }
        movePlayer();
        rotatePlayer();

        fireTimer += Time.deltaTime;
        if(Input.GetMouseButtonDown(0))
        {
            fire();
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            reload();
        }

        if(isReloading)
        {
            weaponTransform.localRotation = Quaternion.Euler(40, 0, 0);
            reloadTimer += Time.deltaTime;
            if(reloadTimer >= reloadTime)
            {
                reloadTimer = 0f;
                shotgunAudio.PlayOneShot(shotgunReload, 0.5f);
                currentAmmo++;
                updateAmmo();
                if(currentAmmo >= maxAmmo)
                {
                    isReloading = false;
                    fireTimer = 0f;
                }
            }
        }
        else
        {
            weaponTransform.localRotation = Quaternion.RotateTowards(weaponTransform.localRotation, Quaternion.identity, Time.deltaTime * 150);
        }


        if(insideBlock && !isCarrying)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                pickupBlock();
            }
        }
        else if(isCarrying && insideShredder)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                leaveToShredder();
            }
        }
        else if(isCarrying)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                dropBlock();
            }
        }
    }
    private void leaveToShredder()
    {
        if(!shredder.isBusy)
        {
            shredder.takeBlock(carryBlock);
            weaponTransform.gameObject.SetActive(true);
            isCarrying = false;
            carryBlock = null;
        }
        
    }
    private void dropBlock()
    {
        carryBlock.transform.SetParent(null);
        Vector3 blockPos = carryBlock.transform.position;
        blockPos += carryBlock.transform.forward * 0.5f;
        blockPos.y = 0.5f;
        
        carryBlock.transform.position = blockPos;

        carryBlock = null;
        isCarrying = false;

        weaponTransform.gameObject.SetActive(true);
    }
    private void pickupBlock()
    {
        Collider[] cols = Physics.OverlapSphere(t.position, 1, 1 << 10);

        if(cols.Length > 0)
        {
            if(cols[0].transform.parent.gameObject.tag == "Block")
            {
                carryBlock = cols[0].transform.parent.gameObject;
                carryBlock.transform.SetParent(carryPoint);
                carryBlock.transform.localPosition = Vector3.zero;
                carryBlock.transform.localRotation = Quaternion.identity;

                isCarrying = true;

                weaponTransform.gameObject.SetActive(false);
            }
        }
    }
    private void fire()
    {
        if(fireTimer < fireTime || isReloading || isCarrying)
        {
            return;
        }
        fireTimer = 0;

        updateAmmo();
        if(currentAmmo >= 0)
        {
            currentAmmo--;
            shotgunAudio.PlayOneShot(shotgunFire);
            for(int i = 0; i < 5; i++)
            {
                Bullet b = Instantiate(bulletPrefab, firePoint.position + (firePoint.forward * Random.value), firePoint.rotation * Quaternion.Euler(0, Random.Range(-5f, 5f), 0));
            }

            weaponTransform.localRotation = Quaternion.Euler(-50, 0, 0);
        }
        else
        {
            reload();
        }
    }
    private void reload()
    {
        if(!isReloading && currentAmmo < maxAmmo && !isCarrying)
        {
            isReloading = true;
        }
    }
    private void movePlayer()
    {
        Vector3 move = new Vector3();
        if(Input.GetKey(KeyCode.W))
        {
            move += (getMoveForwardDir());
        }
        else if(Input.GetKey(KeyCode.S))
        {
            move += (-getMoveForwardDir());
        }
        if(Input.GetKey(KeyCode.A))
        {
            move += (-getMoveRightDir());
        }
        else if(Input.GetKey(KeyCode.D))
        {
            move += (getMoveRightDir());
        }
        move.Normalize();

        move *= getSpeed();
        move += Physics.gravity;

        cc.Move(move);
    }
    private void rotatePlayer()
    {
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit floorHit;

        if(Physics.Raycast(camRay, out floorHit, 100f, 1 << 9))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            t.rotation = newRotation;
        }
    }

    private float getSpeed()
    {
        return speed * (isCarrying ? carrySpeedMul : 1) * Time.deltaTime;
    }

    private Vector3 getMoveForwardDir()
    {
        Vector3 dir = cam.transform.forward;
        dir.y = 0;
        dir.Normalize();

        return dir;
    }
    private Vector3 getMoveRightDir()
    {
        Vector3 dir = cam.transform.right;
        dir.y = 0;
        dir.Normalize();

        return dir;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Shredder")
        {
            insideShredder = true;
        }
        else if(other.tag == "Block")
        {
            insideBlock = true;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Block")
        {
            insideBlock = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Shredder")
        {
            insideShredder = false;
        }
        else if(other.tag == "Block")
        {
            insideBlock = false;
        }
    }

    public void takeDamage(int damage)
    {
        if(!dead)
        {
            hurtAudio.Play();
            health -= damage;
            updateHealth();
            if(health <= 0)
            {
                dead = true;
                gameOverGroup.DOAnchorPos(new Vector2(0, 0), 1f);
                waveLabel.text = "" + EnemyController.instance.wave;
            }
        }
    }

    private void updateAmmo()
    {
        ammoLabel.text = "" + currentAmmo + "/" + maxAmmo;
    }
    private void updateHealth()
    {
        healthLabel.text = "" + health + "/" + "100";
    }

    public void onRestart()
    {
        SceneManager.LoadScene("Game");
    }
    public void onMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
