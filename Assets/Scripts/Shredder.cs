﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Shredder : MonoBehaviour
{
    public ParticleSystem ps;
    public Transform blockPoint;
    public AudioSource audioSource;
    public bool isBusy;

    private Vector3 blockPointStartPos;

    private GameObject block;
	// Use this for initialization
	void Start ()
    {
        blockPointStartPos = blockPoint.localPosition;

    }
	
	// Update is called once per frame
	void Update ()
    {
        if(isBusy && block != null)
        {
            block.transform.localPosition = Vector3.Lerp(block.transform.localPosition, Random.insideUnitSphere * 0.3f, Time.deltaTime * 10);
        }
	}

    public void takeBlock(GameObject go)
    {
        if(!isBusy)
        {
            block = go;

            blockPoint.localPosition = blockPointStartPos;

            isBusy = true;
            block.transform.SetParent(blockPoint);
            block.transform.localPosition = Vector3.zero;
            block.transform.localRotation = Quaternion.identity;
            block.tag = "Untagged";

            for(int i = 0; i < block.transform.childCount; i++)
            {
                block.transform.GetChild(0).tag = "Untagged";
            }

            blockPoint.DOLocalMoveY(-1.7f, 2f).SetEase(Ease.Linear).OnComplete(onComplete);

            ps.Play();
            audioSource.Play();
        }
    }

    private void onComplete()
    {
        isBusy = false;
        Destroy(block);
        block = null;
    }
}
